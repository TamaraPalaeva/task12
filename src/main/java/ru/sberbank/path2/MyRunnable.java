package ru.sberbank.path2;

public class MyRunnable implements Runnable {
    private String name;

    public MyRunnable(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(100);
            System.out.println(name + " - запущено");

            Thread.sleep(100);
            System.out.println(name + " - остановлено");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
