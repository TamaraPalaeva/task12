package ru.sberbank.path2.service;

public interface ExecutionManager {
    Context execute(Runnable callback, Runnable... tasks);
}
