package ru.sberbank.path2.impl;

import ru.sberbank.path2.service.Context;
import ru.sberbank.path2.service.ExecutionManager;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class ExecutionManagerImpl implements ExecutionManager {

    volatile AtomicBoolean isInterrupted = new AtomicBoolean(false);
    volatile AtomicInteger countInterrupt = new AtomicInteger(0);
    volatile AtomicInteger countWell = new AtomicInteger(0);
    volatile AtomicInteger countFailure = new AtomicInteger(0);
    volatile AtomicInteger allTaskCount = new AtomicInteger(0);
    Runnable callback;
    Runnable[] tasksArray;

    @Override
    public Context execute(Runnable callback, Runnable... tasks) {
        this.callback = callback;
        this.tasksArray = tasks;
        ExecutorService executor = Executors.newFixedThreadPool(3);
        Arrays.stream(tasksArray).map(task -> {
            Runnable runnable = new MyRunnableExecution(task, executor);
                    return runnable;
                }
        ).forEach(executor::submit);

        Context context = new Context() {
            @Override
            public int getCompletedTaskCount() {  //возвращает количество тасков, которые на текущий момент успешно выполнились
                return countWell.get();
            }

            @Override
            public int getFailedTaskCount() { //возвращает количество тасков, при выполнении которых произошел Exception
                return countFailure.get();
            }

            @Override
            public int getInterruptedTaskCount() {      //возвращает количество тасков, которые не были выполены из-за отмены (вызовом предыдущего метода)
                return countInterrupt.get();
            }

            @Override
            public void interrupt() {   //отменяет выполнения тасков, которые еще не начали выполняться
                isInterrupted.set(true);
            }

            @Override
            public boolean isFinished() { //вернет true, если все таски были выполнены или отменены, false в противном случае
                return executor.isShutdown();
            }
        };


        executor.shutdown();

        return context;
    }

    private class MyRunnableExecution implements Runnable {

        Runnable task;
        ExecutorService executor;

        public MyRunnableExecution(Runnable task, ExecutorService executor) {
            this.task = task;
            if (executor == null) this.executor = executor;
        }

        @Override
        public void run() {
            if (isInterrupted.get()) {
                countInterrupt.getAndIncrement();
                System.out.println("Завершено принудительно " + Thread.currentThread().getName());
            } else
                try {
                    task.run();
                    countWell.getAndIncrement();
                    System.out.println("Завершено удачно " + countWell.get());
                } catch (Exception e) {
                    countFailure.getAndIncrement();
                    System.out.println("С ошибкой " + countWell.get());
                }
            if (allTaskCount.incrementAndGet() == tasksArray.length) {
                System.out.println("\nTasks закончил работу \nВсего в работе было " + allTaskCount.get());
                callback.run();
                executor.shutdown();
            }
        }
    }

}
