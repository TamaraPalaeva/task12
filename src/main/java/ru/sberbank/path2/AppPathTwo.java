package ru.sberbank.path2;

import ru.sberbank.path2.impl.ExecutionManagerImpl;
import ru.sberbank.path2.service.Context;

public class AppPathTwo {
    public static void main(String[] args) {
        Runnable callback = new MyRunnable("callBack");
        Runnable task6 = new MyRunnable("task");
        Runnable task1 = new MyRunnable("task1");
        Runnable task2 = new MyRunnable("task2");
        Runnable task3 = new MyRunnable("task3");
        Runnable task4 = new MyRunnable("task4");
        Runnable task5 = new MyRunnable("task5");

        ExecutionManagerImpl executionManager = new ExecutionManagerImpl();
        Context context = executionManager.execute(callback, task1, task2, task3, task4, task5, task6);
        context.interrupt();

    }
}
