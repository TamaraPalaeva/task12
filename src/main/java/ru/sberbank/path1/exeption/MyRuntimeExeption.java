package ru.sberbank.path1.exeption;

public class MyRuntimeExeption extends RuntimeException {
    public MyRuntimeExeption(String message) {
        super(message);
    }
}
