package ru.sberbank.path1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AppPathOne {


    public static void main(String[] args) throws Exception {

        Task<String> task = new Task<>(() -> "Отработано потоком " + Thread.currentThread().getName());

        ExecutorService executor = Executors.newFixedThreadPool(8);
        executor.submit(() -> {
            try {
                System.out.println("\nНачалась работа " + Thread.currentThread().getName() + "\n Результат");
                String answer = task.get();
                System.out.println(answer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        executor.submit(() -> {
            try {
                System.out.println("\nНачалась работа " + Thread.currentThread().getName() + "\n Результат");
                String answer = task.get();
                System.out.println(answer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        executor.submit(() -> {
            try {
                System.out.println("\nНачалась работа " + Thread.currentThread().getName() + "\n Результат");
                String answer = task.get();
                System.out.println(answer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        executor.shutdown();


    }

}

