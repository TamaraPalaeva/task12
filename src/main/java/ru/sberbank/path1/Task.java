package ru.sberbank.path1;


import ru.sberbank.path1.exeption.MyRuntimeExeption;

import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Task<T> {
    private Callable<? extends T> callable;
    private volatile T result = null;
    private volatile MyRuntimeExeption exeption;
    private static volatile Logger log = Logger.getLogger(Task.class.getName());

    public Task(Callable<? extends T> callable) {
        this.callable = callable;
    }

    public synchronized Callable<? extends T> getCallable() {
        return callable;
    }

    public synchronized T get() throws InterruptedException {
        if (exeption != null) {
            throw exeption;
        } else if (result == null) {
            synchronized (callable) {
                if (result == null) {
                    try {
                        //log.info("запись");
                        result = callable.call();
                    } catch (Exception e) {
                        exeption = new MyRuntimeExeption("Ошибка в работе потока!");
                        log.log(Level.SEVERE, "Exception: ", exeption);
                        throw exeption;
                    }
                }
            }
        } else log.info("Результат из кэш");
        return result;
    }
}